
package com.raymond.raymondcrud.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raymond.raymondcrud.model.Employee;
/**
 *
 * @author Raymond
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}