/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.raymond.raymondcrud.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.raymond.raymondcrud.model.Employee;
import com.raymond.raymondcrud.exception.ResourceNotFoundException;
import com.raymond.raymondcrud.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 *
 * @author Raymond
 */
@RestController
@RequestMapping("/api/v1")

public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;
 

  
    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId)
        throws ResourceNotFoundException {
        Employee employee = employeeRepository.findById(employeeId)
          .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
        return ResponseEntity.ok().body(employee);
    }

    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
         @Valid @RequestBody Employee employeeDetails) throws ResourceNotFoundException {
        Employee employee = employeeRepository.findById(employeeId)
        .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));

        employee.setEmailId(employeeDetails.getEmailId());
        employee.setLastName(employeeDetails.getLastName());
        employee.setFirstName(employeeDetails.getFirstName());
        final Employee updatedEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok(updatedEmployee);
    }



    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId)
         throws ResourceNotFoundException {
        Employee employee = employeeRepository.findById(employeeId)
       .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));

        employeeRepository.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    
    
    
//   //// Swagger
//    @RequestMapping(value = "/employees", method= RequestMethod.GET)
//    public Iterable list(Model model){
//        Iterable employeeList = employeeRepository.findAll();
//        return employeeList;
//    }
//    @RequestMapping(value = "/employees/{id}", method= RequestMethod.GET)
//      public ResponseEntity<Employee> ShowEmployee(@PathVariable(value = "id") Long employeeId, Model model)
//              throws ResourceNotFoundException {
//       Employee employyeid = employeeRepository.findById(employeeId)
//               .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
//    return ResponseEntity.ok().body(employyeid);
//    }
//
//
//
//     @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public ResponseEntity saveEmployee(@RequestBody Employee employyeadd){
//        employeeRepository.save(employyeadd);
//        return new ResponseEntity("Product saved successfully", HttpStatus.OK);
//    }
//
//
//
//
//
//    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
//    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
//         @Valid @RequestBody Employee employeeup) throws ResourceNotFoundException {
//        Employee storedEmployee = employeeRepository.findById(employeeId)
//        .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
//
//
//        storedEmployee.setFirstName(employeeup.getFirstName());
//        storedEmployee.setLastName(employeeup.getLastName());
//        storedEmployee.setEmailId(employeeup.getEmailId());
//        employeeRepository.save(storedEmployee);
//        return new ResponseEntity("Product updated successfully", HttpStatus.OK);
//    }
//
//
//       @DeleteMapping("/employees/{id}")
//    public ResponseEntity deleteEmployee(@PathVariable(value = "id") Long employeeId)
//         throws ResourceNotFoundException {
//        Employee employee1 = employeeRepository.findById(employeeId)
//       .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + employeeId));
//        employeeRepository.delete(employee1);
//    return new ResponseEntity("Product deleted successfully", HttpStatus.OK);
//    }
//
   
  
}