/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Raymond
 * Created: Nov 2, 2019
 */

CREATE TABLE IF NOT EXISTS `employee` (
 
    `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(20),
    `email` varchar(50),
    `date_of_birth` timestamp
 
)ENGINE=InnoDB DEFAULT CHARSET=UTF8;